EESchema Schematic File Version 4
LIBS:power
LIBS:device
LIBS:74xx
LIBS:audio
LIBS:interface
LIBS:Supply_PCB_V1-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Supply PCB"
Date "2018-08-10"
Rev "V1"
Comp "Cure Mannheim e.V."
Comment1 "Autor: Lukas Neumann"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 600  750  0    59   ~ 0
Gesamtsicherung (Überspannung + Überstrom)
$Comp
L Cure_Symbol:P6SMB30AT3G D1
U 1 1 5BD50063
P 1250 1300
F 0 "D1" V 1554 1428 50  0000 L CNN
F 1 "P6SMB30AT3G" V 1645 1428 50  0000 L CNN
F 2 "P6SMB30AT3G:DIOM5436X247N" H 1700 1300 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/P6SMB6.8AT3-D.PDF" H 1700 1200 50  0001 L CNN
F 4 "600 Watt Peak Power Zener Transient Voltage Suppressors" H 1700 1100 50  0001 L CNN "Description"
F 5 "2.47" H 1700 1000 50  0001 L CNN "Height"
F 6 "ON Semiconductor" H 1700 900 50  0001 L CNN "Manufacturer_Name"
F 7 "P6SMB30AT3G" H 1700 800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "8079919" H 1700 700 50  0001 L CNN "RS Part Number"
F 9 "70467610" H 1700 500 50  0001 L CNN "Allied_Number"
F 10 "http://www.alliedelec.com/on-semiconductor-p6smb30at3g/70467610/" H 1700 400 50  0001 L CNN "Allied Price/Stock"
F 11 "P6SMB30AT3G" H 1700 300 50  0001 L CNN "Arrow Part Number"
	1    1250 1300
	0    1    1    0   
$EndComp
$Comp
L power:+24V #PWR0101
U 1 1 5BD502FA
P 600 1250
F 0 "#PWR0101" H 600 1100 50  0001 C CNN
F 1 "+24V" H 615 1423 50  0000 C CNN
F 2 "" H 600 1250 50  0001 C CNN
F 3 "" H 600 1250 50  0001 C CNN
	1    600  1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	600  1250 600  1350
Wire Wire Line
	600  1350 750  1350
Wire Wire Line
	1050 1350 1250 1350
Wire Wire Line
	1250 1350 1250 1400
Text GLabel 2200 1350 2    50   Input ~ 0
24V_after_Fuse
$Comp
L Device:Q_NMOS_DGS Q1
U 1 1 5BD50724
P 1850 1450
F 0 "Q1" V 2193 1450 50  0000 C CNN
F 1 "Q_NMOS_DGS" V 2102 1450 50  0000 C CNN
F 2 "" H 2050 1550 50  0001 C CNN
F 3 "~" H 1850 1450 50  0001 C CNN
	1    1850 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 1350 1250 1350
Connection ~ 1250 1350
$Comp
L Device:R R5
U 1 1 5BD507DB
P 1250 2150
F 0 "R5" H 1320 2196 50  0000 L CNN
F 1 "R" H 1320 2105 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 1180 2150 50  0001 C CNN
F 3 "~" H 1250 2150 50  0001 C CNN
	1    1250 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1900 1250 1950
Wire Wire Line
	1250 1950 1850 1950
Wire Wire Line
	1850 1950 1850 1650
Wire Wire Line
	1250 1950 1250 2000
Connection ~ 1250 1950
$Comp
L power:GND #PWR0102
U 1 1 5BD508F3
P 1250 2350
F 0 "#PWR0102" H 1250 2100 50  0001 C CNN
F 1 "GND" H 1255 2177 50  0000 C CNN
F 2 "" H 1250 2350 50  0001 C CNN
F 3 "" H 1250 2350 50  0001 C CNN
	1    1250 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 2300 1250 2350
$Comp
L Cure_Symbol:RXEF005 F1
U 1 1 5BD50AA3
P 1050 6650
F 0 "F1" H 1550 6875 50  0000 C CNN
F 1 "RXEF005" H 1550 6784 50  0000 C CNN
F 2 "RXEF005:HDRV2W60P0X505_1X2_800X300X830P" H 1700 6750 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/RXEF005.pdf" H 1750 6650 50  0001 L CNN
F 4 "LITTELFUSE - RXEF005 - POLYSWITCH, RADIAL, 0.05A" H 1700 6550 50  0001 L CNN "Description"
F 5 "8.3" H 1700 6450 50  0001 L CNN "Height"
F 6 "LITTELFUSE" H 1700 6350 50  0001 L CNN "Manufacturer_Name"
F 7 "RXEF005" H 1700 6250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "5176556P" H 1700 6150 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/5176556P" H 1700 6050 50  0001 L CNN "RS Price/Stock"
F 10 "70059852" H 1700 5950 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/te-connectivity-rxef005/70059852/" H 1700 5850 50  0001 L CNN "Allied Price/Stock"
F 12 "RXEF005" H 1700 5750 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/rxef005/littelfuse" H 1700 5650 50  0001 L CNN "Arrow Price/Stock"
	1    1050 6650
	1    0    0    -1  
$EndComp
$Comp
L Cure_Symbol:SML-D13FWT86 LED4
U 1 1 5BD5C692
P 2150 2200
F 0 "LED4" V 2446 2032 50  0000 R CNN
F 1 "SML-D13FWT86" V 2355 2032 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 2650 2350 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 2650 2250 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 2650 2150 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 2650 1950 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 2650 1850 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 2650 1750 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 2650 1650 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 2650 1550 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 2650 1450 50  0001 L BNN "Arrow Price/Stock"
	1    2150 2200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2150 1750 2150 1800
Wire Wire Line
	2150 2100 2150 2150
Text GLabel 1300 6650 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED1
U 1 1 5BD5D768
P 2300 7150
F 0 "LED1" H 2750 7050 50  0000 R CNN
F 1 "SML-D13FWT86" H 3000 7250 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 2800 7300 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 2800 7200 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 2800 7100 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 2800 6900 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 2800 6800 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 2800 6700 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 2800 6600 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 2800 6500 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 2800 6400 50  0001 L BNN "Arrow Price/Stock"
	1    2300 7150
	-1   0    0    1   
$EndComp
Text GLabel 2000 6650 2    50   Input ~ 0
Disch_Circ_Supply
Wire Wire Line
	1300 6650 1350 6650
Wire Wire Line
	1750 6650 2000 6650
Wire Wire Line
	2200 7150 2400 7150
Wire Wire Line
	1750 7050 1750 7150
Wire Wire Line
	1750 7150 1900 7150
$Comp
L Cure_Symbol:RXEF005 F5
U 1 1 5BD69B36
P 3450 1050
F 0 "F5" H 3950 1275 50  0000 C CNN
F 1 "RXEF005" H 3950 1184 50  0000 C CNN
F 2 "RXEF005:HDRV2W60P0X505_1X2_800X300X830P" H 4100 1150 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/RXEF005.pdf" H 4150 1050 50  0001 L CNN
F 4 "LITTELFUSE - RXEF005 - POLYSWITCH, RADIAL, 0.05A" H 4100 950 50  0001 L CNN "Description"
F 5 "8.3" H 4100 850 50  0001 L CNN "Height"
F 6 "LITTELFUSE" H 4100 750 50  0001 L CNN "Manufacturer_Name"
F 7 "RXEF005" H 4100 650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "5176556P" H 4100 550 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/5176556P" H 4100 450 50  0001 L CNN "RS Price/Stock"
F 10 "70059852" H 4100 350 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/te-connectivity-rxef005/70059852/" H 4100 250 50  0001 L CNN "Allied Price/Stock"
F 12 "RXEF005" H 4100 150 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/rxef005/littelfuse" H 4100 50  50  0001 L CNN "Arrow Price/Stock"
	1    3450 1050
	1    0    0    -1  
$EndComp
Text GLabel 3700 1050 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED5
U 1 1 5BD69B57
P 4700 1550
F 0 "LED5" H 5150 1450 50  0000 R CNN
F 1 "SML-D13FWT86" H 5400 1650 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 5200 1700 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 5200 1600 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 5200 1500 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 5200 1300 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 5200 1200 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 5200 1100 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 5200 1000 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 5200 900 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 5200 800 50  0001 L BNN "Arrow Price/Stock"
	1    4700 1550
	-1   0    0    1   
$EndComp
Text GLabel 4400 1050 2    50   Input ~ 0
Pre_Circ_Supply
Wire Wire Line
	3700 1050 3750 1050
Wire Wire Line
	4150 1050 4400 1050
Wire Wire Line
	4600 1550 4800 1550
Wire Wire Line
	4150 1450 4150 1550
Wire Wire Line
	4150 1550 4300 1550
$Comp
L Cure_Symbol:RXEF005 F8
U 1 1 5BD69E2A
P 1000 2750
F 0 "F8" H 1500 2975 50  0000 C CNN
F 1 "RXEF005" H 1500 2884 50  0000 C CNN
F 2 "RXEF005:HDRV2W60P0X505_1X2_800X300X830P" H 1650 2850 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/RXEF005.pdf" H 1700 2750 50  0001 L CNN
F 4 "LITTELFUSE - RXEF005 - POLYSWITCH, RADIAL, 0.05A" H 1650 2650 50  0001 L CNN "Description"
F 5 "8.3" H 1650 2550 50  0001 L CNN "Height"
F 6 "LITTELFUSE" H 1650 2450 50  0001 L CNN "Manufacturer_Name"
F 7 "RXEF005" H 1650 2350 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "5176556P" H 1650 2250 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/5176556P" H 1650 2150 50  0001 L CNN "RS Price/Stock"
F 10 "70059852" H 1650 2050 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/te-connectivity-rxef005/70059852/" H 1650 1950 50  0001 L CNN "Allied Price/Stock"
F 12 "RXEF005" H 1650 1850 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/rxef005/littelfuse" H 1650 1750 50  0001 L CNN "Arrow Price/Stock"
	1    1000 2750
	1    0    0    -1  
$EndComp
Text GLabel 1250 2750 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED8
U 1 1 5BD69E4B
P 2250 3250
F 0 "LED8" H 2700 3150 50  0000 R CNN
F 1 "SML-D13FWT86" H 2950 3350 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 2750 3400 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 2750 3300 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 2750 3200 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 2750 3000 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 2750 2900 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 2750 2800 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 2750 2700 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 2750 2600 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 2750 2500 50  0001 L BNN "Arrow Price/Stock"
	1    2250 3250
	-1   0    0    1   
$EndComp
Text GLabel 1950 2750 2    50   Input ~ 0
SC_PCB_Supply
Wire Wire Line
	1250 2750 1300 2750
Wire Wire Line
	1700 2750 1950 2750
Wire Wire Line
	2150 3250 2350 3250
Wire Wire Line
	1700 3150 1700 3250
Wire Wire Line
	1700 3250 1850 3250
$Comp
L Cure_Symbol:RXEF005 F17
U 1 1 5BD6A980
P 950 3650
F 0 "F17" H 1450 3875 50  0000 C CNN
F 1 "RXEF005" H 1450 3784 50  0000 C CNN
F 2 "RXEF005:HDRV2W60P0X505_1X2_800X300X830P" H 1600 3750 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/RXEF005.pdf" H 1650 3650 50  0001 L CNN
F 4 "LITTELFUSE - RXEF005 - POLYSWITCH, RADIAL, 0.05A" H 1600 3550 50  0001 L CNN "Description"
F 5 "8.3" H 1600 3450 50  0001 L CNN "Height"
F 6 "LITTELFUSE" H 1600 3350 50  0001 L CNN "Manufacturer_Name"
F 7 "RXEF005" H 1600 3250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "5176556P" H 1600 3150 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/5176556P" H 1600 3050 50  0001 L CNN "RS Price/Stock"
F 10 "70059852" H 1600 2950 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/te-connectivity-rxef005/70059852/" H 1600 2850 50  0001 L CNN "Allied Price/Stock"
F 12 "RXEF005" H 1600 2750 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/rxef005/littelfuse" H 1600 2650 50  0001 L CNN "Arrow Price/Stock"
	1    950  3650
	1    0    0    -1  
$EndComp
Text GLabel 1200 3650 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED17
U 1 1 5BD6A9A1
P 2200 4150
F 0 "LED17" H 2650 4050 50  0000 R CNN
F 1 "SML-D13FWT86" H 2900 4250 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 2700 4300 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 2700 4200 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 2700 4100 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 2700 3900 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 2700 3800 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 2700 3700 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 2700 3600 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 2700 3500 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 2700 3400 50  0001 L BNN "Arrow Price/Stock"
	1    2200 4150
	-1   0    0    1   
$EndComp
Text GLabel 1900 3650 2    50   Input ~ 0
LED_PCB_Supply
Wire Wire Line
	1200 3650 1250 3650
Wire Wire Line
	1650 3650 1900 3650
Wire Wire Line
	2100 4150 2300 4150
Wire Wire Line
	1650 4050 1650 4150
Wire Wire Line
	1650 4150 1800 4150
$Comp
L Cure_Symbol:RXEF005 F20
U 1 1 5BD6B103
P 950 4850
F 0 "F20" H 1450 5075 50  0000 C CNN
F 1 "RXEF005" H 1450 4984 50  0000 C CNN
F 2 "RXEF005:HDRV2W60P0X505_1X2_800X300X830P" H 1600 4950 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/RXEF005.pdf" H 1650 4850 50  0001 L CNN
F 4 "LITTELFUSE - RXEF005 - POLYSWITCH, RADIAL, 0.05A" H 1600 4750 50  0001 L CNN "Description"
F 5 "8.3" H 1600 4650 50  0001 L CNN "Height"
F 6 "LITTELFUSE" H 1600 4550 50  0001 L CNN "Manufacturer_Name"
F 7 "RXEF005" H 1600 4450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "5176556P" H 1600 4350 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/5176556P" H 1600 4250 50  0001 L CNN "RS Price/Stock"
F 10 "70059852" H 1600 4150 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/te-connectivity-rxef005/70059852/" H 1600 4050 50  0001 L CNN "Allied Price/Stock"
F 12 "RXEF005" H 1600 3950 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/rxef005/littelfuse" H 1600 3850 50  0001 L CNN "Arrow Price/Stock"
	1    950  4850
	1    0    0    -1  
$EndComp
Text GLabel 1200 4850 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED20
U 1 1 5BD6B124
P 2200 5350
F 0 "LED20" H 2650 5250 50  0000 R CNN
F 1 "SML-D13FWT86" H 2900 5450 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 2700 5500 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 2700 5400 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 2700 5300 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 2700 5100 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 2700 5000 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 2700 4900 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 2700 4800 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 2700 4700 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 2700 4600 50  0001 L BNN "Arrow Price/Stock"
	1    2200 5350
	-1   0    0    1   
$EndComp
Text GLabel 1900 4850 2    50   Input ~ 0
IMD_Supply
Wire Wire Line
	1200 4850 1250 4850
Wire Wire Line
	1650 4850 1900 4850
Wire Wire Line
	2100 5350 2300 5350
Wire Wire Line
	1650 5250 1650 5350
Wire Wire Line
	1650 5350 1800 5350
$Comp
L Cure_Symbol:RXEF005 F18
U 1 1 5BD6D504
P 3800 6750
F 0 "F18" H 4300 6975 50  0000 C CNN
F 1 "RXEF005" H 4300 6884 50  0000 C CNN
F 2 "RXEF005:HDRV2W60P0X505_1X2_800X300X830P" H 4450 6850 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/RXEF005.pdf" H 4500 6750 50  0001 L CNN
F 4 "LITTELFUSE - RXEF005 - POLYSWITCH, RADIAL, 0.05A" H 4450 6650 50  0001 L CNN "Description"
F 5 "8.3" H 4450 6550 50  0001 L CNN "Height"
F 6 "LITTELFUSE" H 4450 6450 50  0001 L CNN "Manufacturer_Name"
F 7 "RXEF005" H 4450 6350 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "5176556P" H 4450 6250 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/5176556P" H 4450 6150 50  0001 L CNN "RS Price/Stock"
F 10 "70059852" H 4450 6050 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/te-connectivity-rxef005/70059852/" H 4450 5950 50  0001 L CNN "Allied Price/Stock"
F 12 "RXEF005" H 4450 5850 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/rxef005/littelfuse" H 4450 5750 50  0001 L CNN "Arrow Price/Stock"
	1    3800 6750
	1    0    0    -1  
$EndComp
Text GLabel 4050 6750 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED18
U 1 1 5BD6D525
P 5050 7250
F 0 "LED18" H 5500 7150 50  0000 R CNN
F 1 "SML-D13FWT86" H 5750 7350 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 5550 7400 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 5550 7300 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 5550 7200 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 5550 7000 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 5550 6900 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 5550 6800 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 5550 6700 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 5550 6600 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 5550 6500 50  0001 L BNN "Arrow Price/Stock"
	1    5050 7250
	-1   0    0    1   
$EndComp
Text GLabel 4750 6750 2    50   Input ~ 0
µC_Chain_Supply
Wire Wire Line
	4050 6750 4100 6750
Wire Wire Line
	4500 6750 4750 6750
Wire Wire Line
	4950 7250 5150 7250
Wire Wire Line
	4500 7150 4500 7250
Wire Wire Line
	4500 7250 4650 7250
$Comp
L Cure_Symbol:RXEF005 F21
U 1 1 5BD6E5D3
P 1000 5650
F 0 "F21" H 1500 5875 50  0000 C CNN
F 1 "RXEF005" H 1500 5784 50  0000 C CNN
F 2 "RXEF005:HDRV2W60P0X505_1X2_800X300X830P" H 1650 5750 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/RXEF005.pdf" H 1700 5650 50  0001 L CNN
F 4 "LITTELFUSE - RXEF005 - POLYSWITCH, RADIAL, 0.05A" H 1650 5550 50  0001 L CNN "Description"
F 5 "8.3" H 1650 5450 50  0001 L CNN "Height"
F 6 "LITTELFUSE" H 1650 5350 50  0001 L CNN "Manufacturer_Name"
F 7 "RXEF005" H 1650 5250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "5176556P" H 1650 5150 50  0001 L CNN "RS Part Number"
F 9 "http://uk.rs-online.com/web/p/products/5176556P" H 1650 5050 50  0001 L CNN "RS Price/Stock"
F 10 "70059852" H 1650 4950 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/te-connectivity-rxef005/70059852/" H 1650 4850 50  0001 L CNN "Allied Price/Stock"
F 12 "RXEF005" H 1650 4750 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/rxef005/littelfuse" H 1650 4650 50  0001 L CNN "Arrow Price/Stock"
	1    1000 5650
	1    0    0    -1  
$EndComp
Text GLabel 1250 5650 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED21
U 1 1 5BD6E5F4
P 2250 6150
F 0 "LED21" H 2700 6050 50  0000 R CNN
F 1 "SML-D13FWT86" H 2950 6250 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 2750 6300 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 2750 6200 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 2750 6100 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 2750 5900 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 2750 5800 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 2750 5700 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 2750 5600 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 2750 5500 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 2750 5400 50  0001 L BNN "Arrow Price/Stock"
	1    2250 6150
	-1   0    0    1   
$EndComp
Text GLabel 1950 5650 2    50   Input ~ 0
TSAL_PCB_Supply
Wire Wire Line
	1250 5650 1300 5650
Wire Wire Line
	1700 5650 1950 5650
Wire Wire Line
	2150 6150 2350 6150
Wire Wire Line
	1700 6050 1700 6150
Wire Wire Line
	1700 6150 1850 6150
Text GLabel 6550 5000 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED10
U 1 1 5BD70E21
P 7550 5500
F 0 "LED10" H 8000 5400 50  0000 R CNN
F 1 "SML-D13FWT86" H 8250 5600 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 8050 5650 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 8050 5550 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 8050 5450 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 8050 5250 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 8050 5150 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 8050 5050 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 8050 4950 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 8050 4850 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 8050 4750 50  0001 L BNN "Arrow Price/Stock"
	1    7550 5500
	-1   0    0    1   
$EndComp
Text GLabel 7250 5000 2    50   Input ~ 0
Pump_Supply
Wire Wire Line
	6550 5000 6600 5000
Wire Wire Line
	7450 5500 7650 5500
Wire Wire Line
	7000 5500 7150 5500
$Comp
L Device:Fuse F10
U 1 1 5BD724D9
P 6750 5000
F 0 "F10" V 6553 5000 50  0000 C CNN
F 1 "0312012.MXP" V 6644 5000 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 6680 5000 50  0001 C CNN
F 3 "~" H 6750 5000 50  0001 C CNN
	1    6750 5000
	0    1    1    0   
$EndComp
Text GLabel 4050 4750 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED15
U 1 1 5BD73D35
P 5050 5250
F 0 "LED15" H 5500 5150 50  0000 R CNN
F 1 "SML-D13FWT86" H 5750 5350 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 5550 5400 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 5550 5300 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 5550 5200 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 5550 5000 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 5550 4900 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 5550 4800 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 5550 4700 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 5550 4600 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 5550 4500 50  0001 L BNN "Arrow Price/Stock"
	1    5050 5250
	-1   0    0    1   
$EndComp
Text GLabel 4750 4750 2    50   Input ~ 0
Inverter1_Supply
Wire Wire Line
	4050 4750 4100 4750
Wire Wire Line
	4950 5250 5150 5250
Wire Wire Line
	4500 5150 4500 5250
Wire Wire Line
	4500 5250 4650 5250
$Comp
L Device:Fuse F15
U 1 1 5BD73D42
P 4250 4750
F 0 "F15" V 4053 4750 50  0000 C CNN
F 1 "03121.25MXP" V 4144 4750 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 4180 4750 50  0001 C CNN
F 3 "~" H 4250 4750 50  0001 C CNN
	1    4250 4750
	0    1    1    0   
$EndComp
Text GLabel 4050 5750 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED19
U 1 1 5BD7968D
P 5050 6250
F 0 "LED19" H 5500 6150 50  0000 R CNN
F 1 "SML-D13FWT86" H 5750 6350 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 5550 6400 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 5550 6300 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 5550 6200 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 5550 6000 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 5550 5900 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 5550 5800 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 5550 5700 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 5550 5600 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 5550 5500 50  0001 L BNN "Arrow Price/Stock"
	1    5050 6250
	-1   0    0    1   
$EndComp
Text GLabel 4750 5750 2    50   Input ~ 0
Inverter2_Supply
Wire Wire Line
	4050 5750 4100 5750
Wire Wire Line
	4950 6250 5150 6250
Wire Wire Line
	4500 6150 4500 6250
Wire Wire Line
	4500 6250 4650 6250
$Comp
L Device:Fuse F19
U 1 1 5BD7969A
P 4250 5750
F 0 "F19" V 4053 5750 50  0000 C CNN
F 1 "03121.25MXP" V 4144 5750 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 4180 5750 50  0001 C CNN
F 3 "~" H 4250 5750 50  0001 C CNN
	1    4250 5750
	0    1    1    0   
$EndComp
Text GLabel 6400 950  0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED3
U 1 1 5BD7B756
P 7400 1450
F 0 "LED3" H 7850 1350 50  0000 R CNN
F 1 "SML-D13FWT86" H 8100 1550 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 7900 1600 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 7900 1500 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 7900 1400 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 7900 1200 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 7900 1100 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 7900 1000 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 7900 900 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 7900 800 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 7900 700 50  0001 L BNN "Arrow Price/Stock"
	1    7400 1450
	-1   0    0    1   
$EndComp
Text GLabel 7100 950  2    50   Input ~ 0
Raspberry_Supply
Wire Wire Line
	6400 950  6450 950 
Wire Wire Line
	7300 1450 7500 1450
Wire Wire Line
	6850 1350 6850 1450
Wire Wire Line
	6850 1450 7000 1450
$Comp
L Device:Fuse F3
U 1 1 5BD7B763
P 6600 950
F 0 "F3" V 6403 950 50  0000 C CNN
F 1 "03121.25MXP" V 6494 950 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 6530 950 50  0001 C CNN
F 3 "~" H 6600 950 50  0001 C CNN
	1    6600 950 
	0    1    1    0   
$EndComp
Text GLabel 6450 3950 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED7
U 1 1 5BD7DA27
P 7450 4450
F 0 "LED7" H 7900 4350 50  0000 R CNN
F 1 "SML-D13FWT86" H 8150 4550 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 7950 4600 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 7950 4500 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 7950 4400 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 7950 4200 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 7950 4100 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 7950 4000 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 7950 3900 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 7950 3800 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 7950 3700 50  0001 L BNN "Arrow Price/Stock"
	1    7450 4450
	-1   0    0    1   
$EndComp
Text GLabel 7150 3950 2    50   Input ~ 0
Fan1_Supply
Wire Wire Line
	6450 3950 6500 3950
Wire Wire Line
	7350 4450 7550 4450
Wire Wire Line
	6900 4350 6900 4450
Wire Wire Line
	6900 4450 7050 4450
$Comp
L Device:Fuse F7
U 1 1 5BD7DA34
P 6650 3950
F 0 "F7" V 6453 3950 50  0000 C CNN
F 1 "0312012.MXP" V 6544 3950 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 6580 3950 50  0001 C CNN
F 3 "~" H 6650 3950 50  0001 C CNN
	1    6650 3950
	0    1    1    0   
$EndComp
Text GLabel 6400 1800 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED11
U 1 1 5BD8020A
P 7400 2300
F 0 "LED11" H 7850 2200 50  0000 R CNN
F 1 "SML-D13FWT86" H 8100 2400 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 7900 2450 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 7900 2350 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 7900 2250 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 7900 2050 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 7900 1950 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 7900 1850 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 7900 1750 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 7900 1650 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 7900 1550 50  0001 L BNN "Arrow Price/Stock"
	1    7400 2300
	-1   0    0    1   
$EndComp
Text GLabel 7100 1800 2    50   Input ~ 0
Energy_Meter_Supply
Wire Wire Line
	6400 1800 6450 1800
Wire Wire Line
	7300 2300 7500 2300
Wire Wire Line
	6850 2200 6850 2300
Wire Wire Line
	6850 2300 7000 2300
$Comp
L Device:Fuse F11
U 1 1 5BD80217
P 6600 1800
F 0 "F11" V 6403 1800 50  0000 C CNN
F 1 "0312012.MXP" V 6494 1800 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 6530 1800 50  0001 C CNN
F 3 "~" H 6600 1800 50  0001 C CNN
	1    6600 1800
	0    1    1    0   
$EndComp
Wire Notes Line
	3050 650  3000 650 
Text GLabel 4000 2950 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED2
U 1 1 5BD9E631
P 5000 3450
F 0 "LED2" H 5450 3350 50  0000 R CNN
F 1 "SML-D13FWT86" H 5700 3550 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 5500 3600 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 5500 3500 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 5500 3400 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 5500 3200 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 5500 3100 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 5500 3000 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 5500 2900 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 5500 2800 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 5500 2700 50  0001 L BNN "Arrow Price/Stock"
	1    5000 3450
	-1   0    0    1   
$EndComp
Text GLabel 4700 2950 2    50   Input ~ 0
Brakelight_Supply
Wire Wire Line
	4000 2950 4050 2950
Wire Wire Line
	4900 3450 5100 3450
Wire Wire Line
	4450 3350 4450 3450
Wire Wire Line
	4450 3450 4600 3450
$Comp
L Device:Fuse F2
U 1 1 5BD9E63E
P 4200 2950
F 0 "F2" V 4003 2950 50  0000 C CNN
F 1 "031201.5MXP" V 4094 2950 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 4130 2950 50  0001 C CNN
F 3 "~" H 4200 2950 50  0001 C CNN
	1    4200 2950
	0    1    1    0   
$EndComp
Text GLabel 4000 3850 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED6
U 1 1 5BDA23CB
P 5000 4350
F 0 "LED6" H 5450 4250 50  0000 R CNN
F 1 "SML-D13FWT86" H 5700 4450 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 5500 4500 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 5500 4400 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 5500 4300 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 5500 4100 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 5500 4000 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 5500 3900 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 5500 3800 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 5500 3700 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 5500 3600 50  0001 L BNN "Arrow Price/Stock"
	1    5000 4350
	-1   0    0    1   
$EndComp
Text GLabel 4700 3850 2    50   Input ~ 0
ES910_Supply
Wire Wire Line
	4000 3850 4050 3850
Wire Wire Line
	4900 4350 5100 4350
Wire Wire Line
	4450 4250 4450 4350
Wire Wire Line
	4450 4350 4600 4350
$Comp
L Device:Fuse F6
U 1 1 5BDA23D8
P 4200 3850
F 0 "F6" V 4003 3850 50  0000 C CNN
F 1 "03121.25MXP" V 4094 3850 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 4130 3850 50  0001 C CNN
F 3 "~" H 4200 3850 50  0001 C CNN
	1    4200 3850
	0    1    1    0   
$EndComp
Text GLabel 3900 2000 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED12
U 1 1 5BDC1FC3
P 4900 2500
F 0 "LED12" H 5350 2400 50  0000 R CNN
F 1 "SML-D13FWT86" H 5600 2600 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 5400 2650 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 5400 2550 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 5400 2450 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 5400 2250 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 5400 2150 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 5400 2050 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 5400 1950 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 5400 1850 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 5400 1750 50  0001 L BNN "Arrow Price/Stock"
	1    4900 2500
	-1   0    0    1   
$EndComp
Text GLabel 4600 2000 2    50   Input ~ 0
SC_Supply
Wire Wire Line
	3900 2000 3950 2000
Wire Wire Line
	4800 2500 5000 2500
Wire Wire Line
	4350 2400 4350 2500
Wire Wire Line
	4350 2500 4500 2500
$Comp
L Device:Fuse F12
U 1 1 5BDC1FD0
P 4100 2000
F 0 "F12" V 3903 2000 50  0000 C CNN
F 1 "0312012.MXP" V 3994 2000 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 4030 2000 50  0001 C CNN
F 3 "~" H 4100 2000 50  0001 C CNN
	1    4100 2000
	0    1    1    0   
$EndComp
$Comp
L Device:Fuse F4
U 1 1 5BDCEFBD
P 900 1350
F 0 "F4" V 703 1350 50  0000 C CNN
F 1 "0312035.MXP" V 794 1350 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 830 1350 50  0001 C CNN
F 3 "~" H 900 1350 50  0001 C CNN
	1    900  1350
	0    1    1    0   
$EndComp
Text GLabel 9750 6550 0    50   Input ~ 0
µC_RTDS
$Comp
L Connector:DB25_Female J1
U 1 1 5BE53A29
P 12850 2250
F 0 "J1" H 13029 2296 50  0000 L CNN
F 1 "DB25_Female" V 13150 1950 50  0000 L CNN
F 2 "1-106507-2:11065072" H 12850 2250 50  0001 C CNN
F 3 " ~" H 12850 2250 50  0001 C CNN
	1    12850 2250
	1    0    0    -1  
$EndComp
Text Notes 12400 850  0    59   ~ 0
Supply Output
Text Notes 12600 3900 0    59   ~ 0
Signals
Text GLabel 12450 4100 0    50   Input ~ 0
µC_RTDS
Text GLabel 12550 1050 0    50   Input ~ 0
SC_PCB_Supply
Text GLabel 12550 1450 0    50   Input ~ 0
Disch_Circ_Supply
Text GLabel 12550 1550 0    50   Input ~ 0
Pre_Circ_Supply
$Comp
L Connector:DB15_Female J2
U 1 1 5BE6F631
P 12850 4800
F 0 "J2" H 13005 4846 50  0000 L CNN
F 1 "DB15_Female" H 13005 4755 50  0000 L CNN
F 2 "1-106506-2:11065062" H 12850 4800 50  0001 C CNN
F 3 " ~" H 12850 4800 50  0001 C CNN
	1    12850 4800
	1    0    0    -1  
$EndComp
Text GLabel 12550 1350 0    50   Input ~ 0
TSAL_PCB_Supply
Text GLabel 12550 2050 0    50   Input ~ 0
Raspberry_Supply
Text GLabel 12550 2250 0    50   Input ~ 0
Energy_Meter_Supply
$Comp
L power:GND #PWR0103
U 1 1 5BD5A866
P 2150 2150
F 0 "#PWR0103" H 2150 1900 50  0001 C CNN
F 1 "GND" H 2155 1977 50  0000 C CNN
F 2 "" H 2150 2150 50  0001 C CNN
F 3 "" H 2150 2150 50  0001 C CNN
	1    2150 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5BD6011C
P 2350 3250
F 0 "#PWR0104" H 2350 3000 50  0001 C CNN
F 1 "GND" H 2355 3077 50  0000 C CNN
F 2 "" H 2350 3250 50  0001 C CNN
F 3 "" H 2350 3250 50  0001 C CNN
	1    2350 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5BD6029A
P 2300 4150
F 0 "#PWR0106" H 2300 3900 50  0001 C CNN
F 1 "GND" H 2305 3977 50  0000 C CNN
F 2 "" H 2300 4150 50  0001 C CNN
F 3 "" H 2300 4150 50  0001 C CNN
	1    2300 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5BD68F1B
P 2300 5350
F 0 "#PWR0107" H 2300 5100 50  0001 C CNN
F 1 "GND" H 2305 5177 50  0000 C CNN
F 2 "" H 2300 5350 50  0001 C CNN
F 3 "" H 2300 5350 50  0001 C CNN
	1    2300 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5BD71B9C
P 2350 6150
F 0 "#PWR0108" H 2350 5900 50  0001 C CNN
F 1 "GND" H 2355 5977 50  0000 C CNN
F 2 "" H 2350 6150 50  0001 C CNN
F 3 "" H 2350 6150 50  0001 C CNN
	1    2350 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5BD761DD
P 5150 7250
F 0 "#PWR0109" H 5150 7000 50  0001 C CNN
F 1 "GND" H 5155 7077 50  0000 C CNN
F 2 "" H 5150 7250 50  0001 C CNN
F 3 "" H 5150 7250 50  0001 C CNN
	1    5150 7250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5BD834A0
P 4800 1550
F 0 "#PWR0112" H 4800 1300 50  0001 C CNN
F 1 "GND" H 4805 1377 50  0000 C CNN
F 2 "" H 4800 1550 50  0001 C CNN
F 3 "" H 4800 1550 50  0001 C CNN
	1    4800 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5BD87AE1
P 2400 7150
F 0 "#PWR0113" H 2400 6900 50  0001 C CNN
F 1 "GND" H 2405 6977 50  0000 C CNN
F 2 "" H 2400 7150 50  0001 C CNN
F 3 "" H 2400 7150 50  0001 C CNN
	1    2400 7150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5BD8C122
P 5100 3450
F 0 "#PWR0114" H 5100 3200 50  0001 C CNN
F 1 "GND" H 5105 3277 50  0000 C CNN
F 2 "" H 5100 3450 50  0001 C CNN
F 3 "" H 5100 3450 50  0001 C CNN
	1    5100 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5BD90763
P 5100 4350
F 0 "#PWR0115" H 5100 4100 50  0001 C CNN
F 1 "GND" H 5105 4177 50  0000 C CNN
F 2 "" H 5100 4350 50  0001 C CNN
F 3 "" H 5100 4350 50  0001 C CNN
	1    5100 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5BD94DA4
P 7650 5500
F 0 "#PWR0116" H 7650 5250 50  0001 C CNN
F 1 "GND" H 7655 5327 50  0000 C CNN
F 2 "" H 7650 5500 50  0001 C CNN
F 3 "" H 7650 5500 50  0001 C CNN
	1    7650 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5BD993E5
P 5150 5250
F 0 "#PWR0117" H 5150 5000 50  0001 C CNN
F 1 "GND" H 5155 5077 50  0000 C CNN
F 2 "" H 5150 5250 50  0001 C CNN
F 3 "" H 5150 5250 50  0001 C CNN
	1    5150 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5BD9DA26
P 5150 6250
F 0 "#PWR0118" H 5150 6000 50  0001 C CNN
F 1 "GND" H 5155 6077 50  0000 C CNN
F 2 "" H 5150 6250 50  0001 C CNN
F 3 "" H 5150 6250 50  0001 C CNN
	1    5150 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5BDA2067
P 10500 6900
F 0 "#PWR0119" H 10500 6650 50  0001 C CNN
F 1 "GND" H 10505 6727 50  0000 C CNN
F 2 "" H 10500 6900 50  0001 C CNN
F 3 "" H 10500 6900 50  0001 C CNN
	1    10500 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 6900 10500 6850
$Comp
L power:GND #PWR0121
U 1 1 5BDBC8E2
P 5000 2500
F 0 "#PWR0121" H 5000 2250 50  0001 C CNN
F 1 "GND" H 5005 2327 50  0000 C CNN
F 2 "" H 5000 2500 50  0001 C CNN
F 3 "" H 5000 2500 50  0001 C CNN
	1    5000 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5BDC0FB1
P 7500 2300
F 0 "#PWR0122" H 7500 2050 50  0001 C CNN
F 1 "GND" H 7505 2127 50  0000 C CNN
F 2 "" H 7500 2300 50  0001 C CNN
F 3 "" H 7500 2300 50  0001 C CNN
	1    7500 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 5BDC55F2
P 7550 4450
F 0 "#PWR0123" H 7550 4200 50  0001 C CNN
F 1 "GND" H 7555 4277 50  0000 C CNN
F 2 "" H 7550 4450 50  0001 C CNN
F 3 "" H 7550 4450 50  0001 C CNN
	1    7550 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 5BDC9CC1
P 7500 1450
F 0 "#PWR0124" H 7500 1200 50  0001 C CNN
F 1 "GND" H 7505 1277 50  0000 C CNN
F 2 "" H 7500 1450 50  0001 C CNN
F 3 "" H 7500 1450 50  0001 C CNN
	1    7500 1450
	1    0    0    -1  
$EndComp
$Comp
L Connector:RJ45 J3
U 1 1 5BFC4F7B
P 13000 6150
F 0 "J3" H 12670 6154 50  0000 R CNN
F 1 "RJ45" H 12670 6245 50  0000 R CNN
F 2 "2301994-9:23019949" V 13000 6175 50  0001 C CNN
F 3 "~" V 13000 6175 50  0001 C CNN
	1    13000 6150
	-1   0    0    1   
$EndComp
Wire Wire Line
	12500 6450 12550 6450
Wire Wire Line
	12550 6450 12550 6550
Wire Wire Line
	12550 6550 12600 6550
Wire Wire Line
	12600 6450 12550 6450
Connection ~ 12550 6450
$Comp
L power:GND #PWR0142
U 1 1 5BFE9E37
P 12300 6200
F 0 "#PWR0142" H 12300 5950 50  0001 C CNN
F 1 "GND" H 12305 6027 50  0000 C CNN
F 2 "" H 12300 6200 50  0001 C CNN
F 3 "" H 12300 6200 50  0001 C CNN
	1    12300 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	12600 6350 12550 6350
Wire Wire Line
	12550 6350 12550 6200
Wire Wire Line
	12550 6050 12600 6050
Wire Wire Line
	12300 6200 12550 6200
Connection ~ 12550 6200
Wire Wire Line
	12550 6200 12550 6050
$Comp
L power:GND #PWR0149
U 1 1 5BD74638
P 12600 7050
F 0 "#PWR0149" H 12600 6800 50  0001 C CNN
F 1 "GND" H 12605 6877 50  0000 C CNN
F 2 "" H 12600 7050 50  0001 C CNN
F 3 "" H 12600 7050 50  0001 C CNN
	1    12600 7050
	-1   0    0    1   
$EndComp
Wire Wire Line
	12550 7200 12650 7200
Wire Wire Line
	12650 7100 12600 7100
Wire Wire Line
	12600 7100 12600 7050
$Comp
L Cure_Symbol:XT60PW-M J4
U 1 1 5BD8989C
P 12900 7150
F 0 "J4" H 12794 6813 60  0000 C CNN
F 1 "XT60PW-M" H 12794 6919 60  0000 C CNN
F 2 "xt60pw:XT60PW" H 12900 7150 60  0001 C CNN
F 3 "" H 12900 7150 60  0001 C CNN
	1    12900 7150
	1    0    0    -1  
$EndComp
Wire Notes Line
	10200 550  12600 550 
Text GLabel 6450 2900 0    50   Input ~ 0
24V_after_Fuse
$Comp
L Cure_Symbol:SML-D13FWT86 LED13
U 1 1 5C00822E
P 7450 3400
F 0 "LED13" H 7900 3300 50  0000 R CNN
F 1 "SML-D13FWT86" H 8150 3500 50  0000 R CNN
F 2 "SML-D13FWT86:SMLD15YWT86" H 7950 3550 50  0001 L BNN
F 3 "http://www.rohm.com/web/global/datasheet/SML-D13FW/sml-d13x-e" H 7950 3450 50  0001 L BNN
F 4 "ROHM SML-D13FWT86, SML 566 nm Green LED, 1608 (0603) Milky White SMD package" H 7950 3350 50  0001 L BNN "Description"
F 5 "ROHM Semiconductor" H 7950 3150 50  0001 L BNN "Manufacturer_Name"
F 6 "SML-D13FWT86" H 7950 3050 50  0001 L BNN "Manufacturer_Part_Number"
F 7 "1332874P" H 7950 2950 50  0001 L BNN "RS Part Number"
F 8 "http://uk.rs-online.com/web/p/products/1332874P" H 7950 2850 50  0001 L BNN "RS Price/Stock"
F 9 "SML-D13FWT86" H 7950 2750 50  0001 L BNN "Arrow Part Number"
F 10 "http://uk.rs-online.com/web/p/products/1332874P" H 7950 2650 50  0001 L BNN "Arrow Price/Stock"
	1    7450 3400
	-1   0    0    1   
$EndComp
Text GLabel 7150 2900 2    50   Input ~ 0
Fan2_Supply
Wire Wire Line
	6450 2900 6500 2900
Wire Wire Line
	7350 3400 7550 3400
Wire Wire Line
	6900 3300 6900 3400
Wire Wire Line
	6900 3400 7050 3400
$Comp
L Device:Fuse F13
U 1 1 5C00823A
P 6650 2900
F 0 "F13" V 6453 2900 50  0000 C CNN
F 1 "03121.25MXP" V 6544 2900 50  0000 C CNN
F 2 "03540101ZXGY:03540101ZXGY" V 6580 2900 50  0001 C CNN
F 3 "~" H 6650 2900 50  0001 C CNN
	1    6650 2900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5C008241
P 7550 3400
F 0 "#PWR01" H 7550 3150 50  0001 C CNN
F 1 "GND" H 7555 3227 50  0000 C CNN
F 2 "" H 7550 3400 50  0001 C CNN
F 3 "" H 7550 3400 50  0001 C CNN
	1    7550 3400
	1    0    0    -1  
$EndComp
$Comp
L Cure_Symbol:BC847,235 Q2
U 1 1 5BDEB12C
P 10200 6550
F 0 "Q2" V 10775 6550 50  0000 C CNN
F 1 "BC847,235" V 10684 6550 50  0000 C CNN
F 2 "BC847_235:SOT95P230X110-3N" H 10650 6400 50  0001 L CNN
F 3 "http://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 10650 6300 50  0001 L CNN
F 4 "BC847 series - 45 V, 100 mA NPN general-purpose transistors" H 10650 6200 50  0001 L CNN "Description"
F 5 "1.1" H 10650 6100 50  0001 L CNN "Height"
F 6 "Nexperia" H 10650 6000 50  0001 L CNN "Manufacturer_Name"
F 7 "BC847,235" H 10650 5900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "BC847,235" H 10650 5600 50  0001 L CNN "Arrow Part Number"
F 9 "http://www.arrow.com/en/products/bc847235/nexperia" H 10650 5500 50  0001 L CNN "Arrow Price/Stock"
	1    10200 6550
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R21
U 1 1 5C118965
P 10000 6550
F 0 "R21" V 9804 6550 50  0000 C CNN
F 1 "10k" V 9895 6550 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 10000 6550 50  0001 C CNN
F 3 "~" H 10000 6550 50  0001 C CNN
	1    10000 6550
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 6550 9900 6550
Wire Wire Line
	10100 6550 10200 6550
$Comp
L power:GND #PWR0105
U 1 1 5C1E5EEE
P 10500 5750
F 0 "#PWR0105" H 10500 5500 50  0001 C CNN
F 1 "GND" H 10505 5577 50  0000 C CNN
F 2 "" H 10500 5750 50  0001 C CNN
F 3 "" H 10500 5750 50  0001 C CNN
	1    10500 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 5750 10500 5700
$Comp
L Cure_Symbol:BC847,235 Q5
U 1 1 5C1E5EFB
P 10200 5400
F 0 "Q5" V 10775 5400 50  0000 C CNN
F 1 "BC847,235" V 10684 5400 50  0000 C CNN
F 2 "BC847_235:SOT95P230X110-3N" H 10650 5250 50  0001 L CNN
F 3 "http://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 10650 5150 50  0001 L CNN
F 4 "BC847 series - 45 V, 100 mA NPN general-purpose transistors" H 10650 5050 50  0001 L CNN "Description"
F 5 "1.1" H 10650 4950 50  0001 L CNN "Height"
F 6 "Nexperia" H 10650 4850 50  0001 L CNN "Manufacturer_Name"
F 7 "BC847,235" H 10650 4750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "BC847,235" H 10650 4450 50  0001 L CNN "Arrow Part Number"
F 9 "http://www.arrow.com/en/products/bc847235/nexperia" H 10650 4350 50  0001 L CNN "Arrow Price/Stock"
	1    10200 5400
	1    0    0    -1  
$EndComp
$Comp
L G2RL-1A-E-ASI_DC24:G2RL-1A-E-ASI_DC24 K3
U 1 1 5C1E5F0C
P 10450 5000
F 0 "K3" H 11100 4635 50  0000 C CNN
F 1 "G2RL-1A-E-ASI_DC24" H 11100 4726 50  0000 C CNN
F 2 "G2RL-1A-E-ASI_DC24:G2R-1A4" H 11600 5100 50  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-g2rl.pdf" H 11600 5000 50  0001 L CNN
F 4 "General Purpose Relays 1 form A w/ TV3 24VDC Coil, 16A" H 11600 4900 50  0001 L CNN "Description"
F 5 "" H 11600 4800 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 11600 4700 50  0001 L CNN "Manufacturer_Name"
F 7 "G2RL-1A-E-ASI DC24" H 11600 4600 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 11600 4500 50  0001 L CNN "RS Part Number"
F 9 "" H 11600 4400 50  0001 L CNN "RS Price/Stock"
F 10 "70960682" H 11600 4300 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/omron-electronic-components-g2rl-1a-e-asi-dc24/70960682/" H 11600 4200 50  0001 L CNN "Allied Price/Stock"
F 12 "G2RL-1A-E-ASI DC24" H 11600 4100 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/g2rl-1a-e-asidc24/omron" H 11600 4000 50  0001 L CNN "Arrow Price/Stock"
	1    10450 5000
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R18
U 1 1 5C1E5F13
P 10000 5400
F 0 "R18" V 9804 5400 50  0000 C CNN
F 1 "10k" V 9895 5400 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 10000 5400 50  0001 C CNN
F 3 "~" H 10000 5400 50  0001 C CNN
	1    10000 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 5400 9900 5400
Wire Wire Line
	10100 5400 10200 5400
Wire Wire Line
	9050 5000 9150 5000
Wire Wire Line
	10450 5000 10500 5000
Wire Wire Line
	10500 5000 10500 5100
Wire Wire Line
	10500 4900 10450 4900
Text GLabel 12450 4200 0    50   Input ~ 0
µC_Pump
Text GLabel 12550 2350 0    50   Input ~ 0
RTDS
Wire Wire Line
	12450 4200 12550 4200
Text GLabel 9750 5400 0    50   Input ~ 0
µC_Pump
Text GLabel 9050 5000 0    50   Input ~ 0
Pump_Supply
Text GLabel 10500 4900 2    50   Input ~ 0
Pump_Switched
$Comp
L power:GND #PWR0110
U 1 1 5C2C660F
P 10500 4600
F 0 "#PWR0110" H 10500 4350 50  0001 C CNN
F 1 "GND" H 10505 4427 50  0000 C CNN
F 2 "" H 10500 4600 50  0001 C CNN
F 3 "" H 10500 4600 50  0001 C CNN
	1    10500 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 4600 10500 4550
$Comp
L Cure_Symbol:BC847,235 Q4
U 1 1 5C2C661C
P 10200 4250
F 0 "Q4" V 10775 4250 50  0000 C CNN
F 1 "BC847,235" V 10684 4250 50  0000 C CNN
F 2 "BC847_235:SOT95P230X110-3N" H 10650 4100 50  0001 L CNN
F 3 "http://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 10650 4000 50  0001 L CNN
F 4 "BC847 series - 45 V, 100 mA NPN general-purpose transistors" H 10650 3900 50  0001 L CNN "Description"
F 5 "1.1" H 10650 3800 50  0001 L CNN "Height"
F 6 "Nexperia" H 10650 3700 50  0001 L CNN "Manufacturer_Name"
F 7 "BC847,235" H 10650 3600 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "BC847,235" H 10650 3300 50  0001 L CNN "Arrow Part Number"
F 9 "http://www.arrow.com/en/products/bc847235/nexperia" H 10650 3200 50  0001 L CNN "Arrow Price/Stock"
	1    10200 4250
	1    0    0    -1  
$EndComp
$Comp
L G2RL-1A-E-ASI_DC24:G2RL-1A-E-ASI_DC24 K2
U 1 1 5C2C662D
P 10450 3850
F 0 "K2" H 11100 3485 50  0000 C CNN
F 1 "G2RL-1A-E-ASI_DC24" H 11100 3576 50  0000 C CNN
F 2 "G2RL-1A-E-ASI_DC24:G2R-1A4" H 11600 3950 50  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-g2rl.pdf" H 11600 3850 50  0001 L CNN
F 4 "General Purpose Relays 1 form A w/ TV3 24VDC Coil, 16A" H 11600 3750 50  0001 L CNN "Description"
F 5 "" H 11600 3650 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 11600 3550 50  0001 L CNN "Manufacturer_Name"
F 7 "G2RL-1A-E-ASI DC24" H 11600 3450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 11600 3350 50  0001 L CNN "RS Part Number"
F 9 "" H 11600 3250 50  0001 L CNN "RS Price/Stock"
F 10 "70960682" H 11600 3150 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/omron-electronic-components-g2rl-1a-e-asi-dc24/70960682/" H 11600 3050 50  0001 L CNN "Allied Price/Stock"
F 12 "G2RL-1A-E-ASI DC24" H 11600 2950 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/g2rl-1a-e-asidc24/omron" H 11600 2850 50  0001 L CNN "Arrow Price/Stock"
	1    10450 3850
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R14
U 1 1 5C2C6634
P 10000 4250
F 0 "R14" V 9804 4250 50  0000 C CNN
F 1 "10k" V 9895 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 10000 4250 50  0001 C CNN
F 3 "~" H 10000 4250 50  0001 C CNN
	1    10000 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 4250 9900 4250
Wire Wire Line
	10100 4250 10200 4250
Wire Wire Line
	10450 3850 10500 3850
Wire Wire Line
	10500 3850 10500 3950
Wire Wire Line
	10500 3750 10450 3750
Text GLabel 9750 4250 0    50   Input ~ 0
µC_Fan1
Text GLabel 9000 3850 0    50   Input ~ 0
Fan1_Supply
Text GLabel 10500 3750 2    50   Input ~ 0
Fan1_Switched
$Comp
L power:GND #PWR0111
U 1 1 5C3ABB02
P 10450 3450
F 0 "#PWR0111" H 10450 3200 50  0001 C CNN
F 1 "GND" H 10455 3277 50  0000 C CNN
F 2 "" H 10450 3450 50  0001 C CNN
F 3 "" H 10450 3450 50  0001 C CNN
	1    10450 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 3450 10450 3400
$Comp
L Cure_Symbol:BC847,235 Q3
U 1 1 5C3ABB0F
P 10150 3100
F 0 "Q3" V 10725 3100 50  0000 C CNN
F 1 "BC847,235" V 10634 3100 50  0000 C CNN
F 2 "BC847_235:SOT95P230X110-3N" H 10600 2950 50  0001 L CNN
F 3 "http://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 10600 2850 50  0001 L CNN
F 4 "BC847 series - 45 V, 100 mA NPN general-purpose transistors" H 10600 2750 50  0001 L CNN "Description"
F 5 "1.1" H 10600 2650 50  0001 L CNN "Height"
F 6 "Nexperia" H 10600 2550 50  0001 L CNN "Manufacturer_Name"
F 7 "BC847,235" H 10600 2450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "BC847,235" H 10600 2150 50  0001 L CNN "Arrow Part Number"
F 9 "http://www.arrow.com/en/products/bc847235/nexperia" H 10600 2050 50  0001 L CNN "Arrow Price/Stock"
	1    10150 3100
	1    0    0    -1  
$EndComp
$Comp
L G2RL-1A-E-ASI_DC24:G2RL-1A-E-ASI_DC24 K1
U 1 1 5C3ABB20
P 10400 2700
F 0 "K1" H 11050 2335 50  0000 C CNN
F 1 "G2RL-1A-E-ASI_DC24" H 11050 2426 50  0000 C CNN
F 2 "G2RL-1A-E-ASI_DC24:G2R-1A4" H 11550 2800 50  0001 L CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-g2rl.pdf" H 11550 2700 50  0001 L CNN
F 4 "General Purpose Relays 1 form A w/ TV3 24VDC Coil, 16A" H 11550 2600 50  0001 L CNN "Description"
F 5 "" H 11550 2500 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 11550 2400 50  0001 L CNN "Manufacturer_Name"
F 7 "G2RL-1A-E-ASI DC24" H 11550 2300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 11550 2200 50  0001 L CNN "RS Part Number"
F 9 "" H 11550 2100 50  0001 L CNN "RS Price/Stock"
F 10 "70960682" H 11550 2000 50  0001 L CNN "Allied_Number"
F 11 "https://www.alliedelec.com/omron-electronic-components-g2rl-1a-e-asi-dc24/70960682/" H 11550 1900 50  0001 L CNN "Allied Price/Stock"
F 12 "G2RL-1A-E-ASI DC24" H 11550 1800 50  0001 L CNN "Arrow Part Number"
F 13 "https://www.arrow.com/en/products/g2rl-1a-e-asidc24/omron" H 11550 1700 50  0001 L CNN "Arrow Price/Stock"
	1    10400 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R8
U 1 1 5C3ABB27
P 9950 3100
F 0 "R8" V 9754 3100 50  0000 C CNN
F 1 "10k" V 9845 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 9950 3100 50  0001 C CNN
F 3 "~" H 9950 3100 50  0001 C CNN
	1    9950 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 3100 9850 3100
Wire Wire Line
	10050 3100 10150 3100
Wire Wire Line
	9000 2700 9100 2700
Wire Wire Line
	10400 2700 10450 2700
Wire Wire Line
	10450 2700 10450 2800
Wire Wire Line
	10450 2600 10400 2600
Text GLabel 9700 3100 0    50   Input ~ 0
µC_Fan2
Text GLabel 9000 2700 0    50   Input ~ 0
Fan2_Supply
Text GLabel 10450 2600 2    50   Input ~ 0
Fan2_Switched
Text GLabel 12450 4300 0    50   Input ~ 0
µC_Fan2
Wire Wire Line
	12450 4300 12550 4300
Text GLabel 12450 4400 0    50   Input ~ 0
µC_Fan1
Wire Wire Line
	12450 4400 12550 4400
Text GLabel 12550 2850 0    50   Input ~ 0
Fan1_Switched
Text GLabel 12550 2950 0    50   Input ~ 0
Fan1_Switched
Text GLabel 12550 3150 0    50   Input ~ 0
Fan2_Switched
Text GLabel 12550 3050 0    50   Input ~ 0
Fan2_Switched
Wire Wire Line
	10600 6050 10500 6050
Wire Wire Line
	10500 6050 10500 6250
Wire Notes Line
	11400 550  11400 7850
Wire Notes Line
	500  7850 11400 7850
Wire Wire Line
	6900 5000 7000 5000
$Comp
L Device:R_Small R17
U 1 1 5C5DC8D1
P 7000 5200
F 0 "R17" H 6941 5154 50  0000 R CNN
F 1 "10k" H 6941 5245 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 7000 5200 50  0001 C CNN
F 3 "~" H 7000 5200 50  0001 C CNN
	1    7000 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 5100 7000 5000
Connection ~ 7000 5000
Wire Wire Line
	7000 5000 7250 5000
Wire Wire Line
	7000 5300 7000 5500
Wire Wire Line
	4400 4750 4500 4750
Wire Wire Line
	6800 3950 6900 3950
Wire Wire Line
	6800 2900 6900 2900
Wire Wire Line
	6750 1800 6850 1800
Wire Wire Line
	6750 950  6850 950 
$Comp
L Device:R_Small R1
U 1 1 5C611D8E
P 6850 1250
F 0 "R1" H 6791 1204 50  0000 R CNN
F 1 "10k" H 6791 1295 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 6850 1250 50  0001 C CNN
F 3 "~" H 6850 1250 50  0001 C CNN
	1    6850 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 1150 6850 950 
Connection ~ 6850 950 
Wire Wire Line
	6850 950  7100 950 
$Comp
L Device:R_Small R4
U 1 1 5C62A886
P 6850 2100
F 0 "R4" H 6791 2054 50  0000 R CNN
F 1 "10k" H 6791 2145 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 6850 2100 50  0001 C CNN
F 3 "~" H 6850 2100 50  0001 C CNN
	1    6850 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 2000 6850 1800
Connection ~ 6850 1800
Wire Wire Line
	6850 1800 7100 1800
$Comp
L Device:R_Small R9
U 1 1 5C643A6B
P 6900 3200
F 0 "R9" H 6841 3154 50  0000 R CNN
F 1 "10k" H 6841 3245 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 6900 3200 50  0001 C CNN
F 3 "~" H 6900 3200 50  0001 C CNN
	1    6900 3200
	-1   0    0    1   
$EndComp
Wire Wire Line
	6900 3100 6900 2900
Connection ~ 6900 2900
Wire Wire Line
	6900 2900 7150 2900
$Comp
L Device:R_Small R13
U 1 1 5C65CFBF
P 6900 4250
F 0 "R13" H 6841 4204 50  0000 R CNN
F 1 "10k" H 6841 4295 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 6900 4250 50  0001 C CNN
F 3 "~" H 6900 4250 50  0001 C CNN
	1    6900 4250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6900 4150 6900 3950
Connection ~ 6900 3950
Wire Wire Line
	6900 3950 7150 3950
$Comp
L Device:R_Small R15
U 1 1 5C676A2D
P 4500 5050
F 0 "R15" H 4441 5004 50  0000 R CNN
F 1 "10k" H 4441 5095 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 4500 5050 50  0001 C CNN
F 3 "~" H 4500 5050 50  0001 C CNN
	1    4500 5050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 4950 4500 4750
Connection ~ 4500 4750
Wire Wire Line
	4500 4750 4750 4750
$Comp
L Device:R_Small R20
U 1 1 5C690B17
P 4500 6050
F 0 "R20" H 4441 6004 50  0000 R CNN
F 1 "10k" H 4441 6095 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 4500 6050 50  0001 C CNN
F 3 "~" H 4500 6050 50  0001 C CNN
	1    4500 6050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 5950 4500 5750
Wire Wire Line
	4400 5750 4500 5750
Connection ~ 4500 5750
Wire Wire Line
	4500 5750 4750 5750
Wire Wire Line
	4350 3850 4450 3850
Wire Wire Line
	4350 2950 4450 2950
Wire Wire Line
	2050 1350 2150 1350
$Comp
L Device:R_Small R12
U 1 1 5C6AECB7
P 4450 4150
F 0 "R12" H 4391 4104 50  0000 R CNN
F 1 "10k" H 4391 4195 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 4450 4150 50  0001 C CNN
F 3 "~" H 4450 4150 50  0001 C CNN
	1    4450 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	4450 4050 4450 3850
Connection ~ 4450 3850
Wire Wire Line
	4450 3850 4700 3850
$Comp
L Device:R_Small R10
U 1 1 5C6C9090
P 4450 3250
F 0 "R10" H 4391 3204 50  0000 R CNN
F 1 "10k" H 4391 3295 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 4450 3250 50  0001 C CNN
F 3 "~" H 4450 3250 50  0001 C CNN
	1    4450 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4450 3150 4450 2950
Connection ~ 4450 2950
Wire Wire Line
	4450 2950 4700 2950
$Comp
L Device:R_Small R6
U 1 1 5C6E3751
P 4350 2300
F 0 "R6" H 4291 2254 50  0000 R CNN
F 1 "10k" H 4291 2345 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 4350 2300 50  0001 C CNN
F 3 "~" H 4350 2300 50  0001 C CNN
	1    4350 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 2200 4350 2000
Wire Wire Line
	4250 2000 4350 2000
Connection ~ 4350 2000
Wire Wire Line
	4350 2000 4600 2000
$Comp
L Device:R_Small R2
U 1 1 5C734CF7
P 4150 1350
F 0 "R2" H 4091 1304 50  0000 R CNN
F 1 "10k" H 4091 1395 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 4150 1350 50  0001 C CNN
F 3 "~" H 4150 1350 50  0001 C CNN
	1    4150 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	4150 1250 4150 1050
Connection ~ 4150 1050
$Comp
L Device:R_Small R22
U 1 1 5C74FE4F
P 1750 6950
F 0 "R22" H 1691 6904 50  0000 R CNN
F 1 "10k" H 1691 6995 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 1750 6950 50  0001 C CNN
F 3 "~" H 1750 6950 50  0001 C CNN
	1    1750 6950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1750 6850 1750 6650
Connection ~ 1750 6650
$Comp
L Device:R_Small R3
U 1 1 5C76BA3B
P 2150 1650
F 0 "R3" H 2091 1604 50  0000 R CNN
F 1 "10k" H 2091 1695 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 2150 1650 50  0001 C CNN
F 3 "~" H 2150 1650 50  0001 C CNN
	1    2150 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2150 1550 2150 1350
Connection ~ 2150 1350
Wire Wire Line
	2150 1350 2200 1350
$Comp
L Device:R_Small R7
U 1 1 5C78821C
P 1700 3050
F 0 "R7" H 1641 3004 50  0000 R CNN
F 1 "10k" H 1641 3095 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 1700 3050 50  0001 C CNN
F 3 "~" H 1700 3050 50  0001 C CNN
	1    1700 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 2950 1700 2750
Connection ~ 1700 2750
$Comp
L Device:R_Small R11
U 1 1 5C7A3E1F
P 1650 3950
F 0 "R11" H 1591 3904 50  0000 R CNN
F 1 "10k" H 1591 3995 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 1650 3950 50  0001 C CNN
F 3 "~" H 1650 3950 50  0001 C CNN
	1    1650 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 3850 1650 3650
Connection ~ 1650 3650
$Comp
L Device:R_Small R16
U 1 1 5C7C01D9
P 1650 5150
F 0 "R16" H 1591 5104 50  0000 R CNN
F 1 "10k" H 1591 5195 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 1650 5150 50  0001 C CNN
F 3 "~" H 1650 5150 50  0001 C CNN
	1    1650 5150
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 5050 1650 4850
Connection ~ 1650 4850
$Comp
L Device:R_Small R23
U 1 1 5C7DC66C
P 4500 7050
F 0 "R23" H 4441 7004 50  0000 R CNN
F 1 "10k" H 4441 7095 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 4500 7050 50  0001 C CNN
F 3 "~" H 4500 7050 50  0001 C CNN
	1    4500 7050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 6950 4500 6750
Connection ~ 4500 6750
$Comp
L Device:R_Small R19
U 1 1 5C7F8C68
P 1700 5950
F 0 "R19" H 1641 5904 50  0000 R CNN
F 1 "10k" H 1641 5995 50  0000 R CNN
F 2 "Resistors_SMD:R_1206" H 1700 5950 50  0001 C CNN
F 3 "~" H 1700 5950 50  0001 C CNN
	1    1700 5950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 5850 1700 5650
Connection ~ 1700 5650
Wire Wire Line
	9000 3850 9150 3850
Text GLabel 9100 4900 0    50   Input ~ 0
24V_after_Fuse
Text GLabel 9100 3750 0    50   Input ~ 0
24V_after_Fuse
Text GLabel 9050 2600 0    50   Input ~ 0
24V_after_Fuse
Wire Wire Line
	9100 4900 9150 4900
Wire Wire Line
	9100 3750 9150 3750
Wire Wire Line
	9050 2600 9100 2600
Text GLabel 12550 1250 0    50   Input ~ 0
IMD_Supply
Text GLabel 12550 1150 0    50   Input ~ 0
LED_PCB_Supply
Text GLabel 12550 3450 0    50   Input ~ 0
Pump_Switched
Text GLabel 12550 3350 0    50   Input ~ 0
Pump_Switched
Text GLabel 12550 3250 0    50   Input ~ 0
Pump_Switched
Text GLabel 12550 1650 0    50   Input ~ 0
SC_Supply
Text GLabel 12550 1750 0    50   Input ~ 0
Brakelight_Supply
Text GLabel 12550 1850 0    50   Input ~ 0
ES910_Supply
Text GLabel 12550 1950 0    50   Input ~ 0
Inverter1_Supply
Text GLabel 12550 2150 0    50   Input ~ 0
Inverter2_Supply
Text GLabel 12500 6450 0    50   Input ~ 0
µC_Chain_Supply
Wire Wire Line
	12450 4100 12550 4100
$Comp
L power:+24V #PWR0120
U 1 1 5C0FF2C5
P 12550 7200
F 0 "#PWR0120" H 12550 7050 50  0001 C CNN
F 1 "+24V" H 12565 7373 50  0000 C CNN
F 2 "" H 12550 7200 50  0001 C CNN
F 3 "" H 12550 7200 50  0001 C CNN
	1    12550 7200
	1    0    0    -1  
$EndComp
Text GLabel 1250 1350 1    50   Input ~ 0
Crowbar_fuse
Text GLabel 10600 6050 2    50   Input ~ 0
RTDS
$EndSCHEMATC
